package edu.iastate.jmay.lecture14_callbacks;

public class LibraryClass {
    private ICallback watcher;

    public interface ICallback {
        void callbackFunc();
    }

    public void addWatcher(ICallback c) {
        this.watcher = c;
    }

    public void doSomething() {
        this.watcher.callbackFunc();
    }
}
