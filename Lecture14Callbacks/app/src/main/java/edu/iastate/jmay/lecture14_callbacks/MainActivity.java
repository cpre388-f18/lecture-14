package edu.iastate.jmay.lecture14_callbacks;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements MyFragment.MyInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        // If the Fragment is already loaded, don't load it again.
        if (fm.findFragmentById(R.id.fragLayout) == null) {
            // Set up the fragment
            fm.beginTransaction()
                    .replace(R.id.fragLayout, new MyFragment())
                    .commit();
        }
    }

    /**
     * This is the event receiver that is called by the fragment.
     */
    @Override
    public void onClick() {
        // On an event from the fragment
        TextView tv = findViewById(R.id.mainTextView);
        tv.setText(R.string.event_msg);
    }
}
