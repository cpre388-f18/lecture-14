package edu.iastate.jmay.lecture14_callbacks;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MyFragment extends Fragment {
    /** The instance of the Activity to sent events to. */
    private MyInterface watcher;

    @Override
    public void onAttach(Context context) {
        // This is where the hosting Activity (MainActivity) is stored as a watcher.
        // Notice how the Activity (context) is stored as the interface (MyInterface) to allow
        // calling the onClick() function.
        watcher = (MyInterface) context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my, container, false);
        // Set the button click event handler.
        v.findViewById(R.id.fragButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Send an event to the parent Activity
                watcher.onClick();
            }
        });
        return v;
    }

    public interface MyInterface {
        void onClick();
    }
}
