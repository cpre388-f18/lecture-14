package edu.iastate.jmay.lecture14_callbacks;

public class MyClass implements LibraryClass.ICallback {
    private LibraryClass libraryInstance = new LibraryClass();

    public MyClass() {
        libraryInstance.addWatcher(this);
    }

    public void doSomethingOnLib() {
        libraryInstance.doSomething();
    }

    @Override
    public void callbackFunc() {
        // Do something about the message from the Library
    }
}
