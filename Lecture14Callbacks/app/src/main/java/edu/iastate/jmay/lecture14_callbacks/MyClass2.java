package edu.iastate.jmay.lecture14_callbacks;

public class MyClass2 implements LibraryClass.ICallback {
    private LibraryClass libraryInstance = new LibraryClass();

    public MyClass2() {
        libraryInstance.addWatcher(this);
    }

    public void doSomethingOnLibrary() {
        libraryInstance.doSomething();
    }

    @Override
    public void callbackFunc() {
        // Callback from the library
    }
}
