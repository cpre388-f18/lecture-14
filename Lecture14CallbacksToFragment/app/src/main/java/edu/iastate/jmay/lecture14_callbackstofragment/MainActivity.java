package edu.iastate.jmay.lecture14_callbackstofragment;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    /** A counter of button click events. */
    private int val = 0;
    /** The Fragment instance that will receive events from this Activity. */
    private MainActivityListenerInterface watcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        if (fm.findFragmentById(R.id.fragmentLayout) == null) {
            MyFragment newFrag = new MyFragment();
            fm.beginTransaction()
                    .add(R.id.fragmentLayout, newFrag)
                    .commit();
            // Store the instance of the new Fragment for sending callbacks to.
            watcher = (MainActivityListenerInterface) newFrag;
        } else {
            // Store the existing Fragment instance for event calls later.
            // If we switch out the Fragment at any time, this watcher must be set to the new instance.
            watcher = (MainActivityListenerInterface) fm.findFragmentById(R.id.fragmentLayout);
        }
    }

    public void onButtonClick(View v) {
        val++;
        // Call the callback on the Fragment.
        watcher.onMainEvent(val);
    }

    /** The interface that Fragments must implement to receive the event. */
    public interface MainActivityListenerInterface {
        void onMainEvent(int val);
    }
}
