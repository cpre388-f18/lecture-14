package edu.iastate.jmay.lecture14_callbackstofragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

// NOTICE: this class implements the interface defined in MainActivity.  This is why it may be
// casted on MainActivity:27 to the interface.
public class MyFragment extends Fragment implements MainActivity.MainActivityListenerInterface {
    private TextView fragTextView;

    public MyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my, container, false);
        // Store the instance of the TextView for updating later.
        fragTextView = v.findViewById(R.id.fragTextView);
        return v;
    }

    /**
     * This is the callback that is raised by the hosting Activity (MainActivity).
     * @param val A value to display on the TextView
     */
    @Override
    public void onMainEvent(int val) {
        // Set the TextView to val, so we can see that something happened.
        fragTextView.setText(getContext().getString(R.string.frag_format_str, val));
    }
}
